<p align="center"><a href="" target="_blank"><img src=https://i.postimg.cc/JnScMJJy/Logo.png" width="300"></a></p>
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a></p>

---

# Les images et fichiers css / js

<h4 align="center">Les images, fichiers css / js et assets en générales sont stockés dans le dossier public</h4>
<p>Pour faire appel a ces fichiers sur Laravel, il suffit de copier ça dans un fichier <u><strong>.blade</strong></u> et mettre ton lien.</p>

* Comme ce-ci : 
```php

    {{url('images/Logo.png')}}

```

example :

```php

 <img src="{{url('images/Logo.png')}}" class="img-fluid" width="600"><img>
    

```

<h3>Les styles et js seront edités dans un premier temps dans le dossier public directement</h3>
<h4>Puis par la suite on les modifiera dans le dossier "resources" puis on passera par Webpack pour les compiler bien propres, et la sortie de Webpack se trouvera alors dans le dossier public.</h4>

---

# Les fichier Blade 

 <p>Les fichies blade sont comme des fichier HTML simple, ils sont juste parsés par laravel pour les appels d'images et reconstitution de blocks.</p>

<p>On va séparer les blocks (ce que j'appelle des ) dans le dossier <strong><u>Resources/views/partials/</u></strong></p>

 * <p>Chaque partial sera a terme un chunks dans ModX</p>

 * <p>Les pages (Ressources dans ModX) sont stockées a la racine de <strong><u>"resources/views/"</u></strong></p>

 * <p>les modeles de pages sont stockés dans <strong><u>"resources/views/web"</u></strong></p>
    
    * Les modeles de pages ont besoin d'un espace pour le contenu que ont déclare comme ceci : 

        ```php
        
            @yield('content')
            
        ```

<p>Pour faire apel a un modèle de page dans une page : 

```php

@extends('web/modeleDePage1')

@section('content')
    <h1>Contenue de la page</h1>
@endsection

```

a utiliser une fois en haut de ta page.
<br>
Les balises section et endsection sont nécéssaires pour mettre le contenu dans la page.
</p>

 <p>Pour faire apel a un partial (Chunk) de page dans une pages : 

```php

@include('partials/partial1')

```

</p>

---
# Route

Pour que les urls soient jolies il faut mettre en place des routes. Elles se trouvent dans le fichier web.php dans le dossier "routes"

ça se presente de cette forme la : 

```php

Route::get('/', function () {
    return view('connexions');
});

```

juste apres le GET, le premier paramètre de la fonctions get(), c'est l'url que tu mettra sur ton navigateur pour avoir accès a ta page
et dans le corps de la fonctions c'est la vue "connexions" içi qui est retournée.

<p>En gros : SI l'url est egale a "/" alors tu montres la vue "connexions"</p>

<p>Exemple :</p>

<p>Si l'url est "/groupes" alors tu montres la vue "groupe" : </p>

```php

Route::get('/groupes', function () {
    return view('groupe');
});

```

<p>N'hésite pas a me demander si besoin</p>

<p>ps: si tu veux mettre plusieurs routes, tu les enchaînes comme ceci : </p>

```php

Route::get('/', function () {
    return view('connexions');
});

Route::get('/groupes', function () {
    return view('groupe');
});

Route::get('/inscription', function () {
    return view('inscription');
});

```

---
# Pour le reste n'hésite pas me demander.

## Et je vais commencé a coder regarde ce que j'ai fais, je vais mettre le plus de commentaires possible.

### Hesite pas a corriger mes fautes d'orthographe aussi Ahah
#### c'est fait (The BugFinder)
